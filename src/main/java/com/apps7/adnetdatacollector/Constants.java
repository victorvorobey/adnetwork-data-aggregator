package com.apps7.adnetdatacollector;

/**
 * Holdings constants of the project
 */
public class Constants {

    public static final String BASE_CSV_TO_ENTITY_MAPPER = "BaseCsvToEntityMapper";
    public static final String UMBRELLA_NETWORK_NAME = "AdUmbrella";
}
