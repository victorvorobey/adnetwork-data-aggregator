package com.apps7.adnetdatacollector;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

import static com.apps7.adnetdatacollector.persistence.repository.ApplicationRepository.APPLICATION_BY_NAME_CACHE_KEY;
import static com.apps7.adnetdatacollector.persistence.repository.PlatformRepository.PLATFORM_BY_NAME_CACHE_KEY;

@Configuration
@EnableCaching
public class CacheConfig {

    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        cacheManager.setCaches(Arrays.asList(
                new ConcurrentMapCache(PLATFORM_BY_NAME_CACHE_KEY),
                new ConcurrentMapCache(APPLICATION_BY_NAME_CACHE_KEY)
        ));

        return cacheManager;
    }
}
