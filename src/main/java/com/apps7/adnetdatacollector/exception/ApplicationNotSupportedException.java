package com.apps7.adnetdatacollector.exception;

public class ApplicationNotSupportedException extends RuntimeException {

    public ApplicationNotSupportedException(String message) {
        super(message);
    }

    public ApplicationNotSupportedException(String message, Throwable cause) {
        super(message, cause);
    }
}
