package com.apps7.adnetdatacollector.exception;

public class PlatformNotSupportedException extends RuntimeException {

    public PlatformNotSupportedException(String message) {
        super(message);
    }

    public PlatformNotSupportedException(String message, Throwable cause) {
        super(message, cause);
    }
}
