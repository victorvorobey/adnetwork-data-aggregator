package com.apps7.adnetdatacollector.service;

import com.apps7.adnetdatacollector.persistence.model.DailyReport;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public interface IDataImportService {
    List<DailyReport> importReport(final String adNetworkName, final LocalDate date) throws IOException;
}
