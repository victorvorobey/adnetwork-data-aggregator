package com.apps7.adnetdatacollector.service.impl;

import com.apps7.adnetdatacollector.csv.CSVReportParser;
import com.apps7.adnetdatacollector.persistence.model.AdNetwork;
import com.apps7.adnetdatacollector.persistence.model.DailyReport;
import com.apps7.adnetdatacollector.persistence.repository.AdNetworkRepository;
import com.apps7.adnetdatacollector.persistence.repository.DailyReportRepository;
import com.apps7.adnetdatacollector.service.ICsvSchemaToEntityMapper;
import com.apps7.adnetdatacollector.service.IDataImportService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.apps7.adnetdatacollector.Constants.BASE_CSV_TO_ENTITY_MAPPER;

@Slf4j
@RequiredArgsConstructor(onConstructor = @_(@Autowired))
@Service
public class DataImportService implements IDataImportService {

    private final ApplicationContext appCtx;
    private final AdNetworkRepository adNetworkRepository;
    private final DailyReportRepository dailyReportRepository;

    @Transactional
    @Override
    public List<DailyReport> importReport(final String adNetworkName, final LocalDate date) throws IOException {
        final Optional<AdNetwork> adNetworkOpt = adNetworkRepository.findByName(adNetworkName);
        if (!adNetworkOpt.isPresent()) {
            throw new IllegalArgumentException(String.format("The Advertising Network \"%s\" isn't supported.", adNetworkName));
        }

        final AdNetwork adNetwork = adNetworkOpt.get();
        final String csvSchema = adNetwork.getCsvSchema();
        if (StringUtils.isBlank(csvSchema)) {
            throw new RuntimeException(String.format("CSV Schema is blank for AdNetwork: {}", adNetwork.getName()));
        }

        final String[] csvColumns = csvSchema.split(",");
        CSVReportParser provider = new CSVReportParser(adNetwork, date, csvColumns);
        final List<Map<String, String>> report = provider.run();

        ICsvSchemaToEntityMapper mapper;

        // First, try to get a specific mapper for a specific Ad Network
        if (appCtx.containsBean(adNetworkName)) {
            mapper = appCtx.getBean(adNetworkName, ICsvSchemaToEntityMapper.class);
        } else {
            // and if it doesn't exist, then use default
            mapper = appCtx.getBean(BASE_CSV_TO_ENTITY_MAPPER, ICsvSchemaToEntityMapper.class);
        }

        mapper.setCSVDateFormat(adNetwork.getCsvDateFormat());

        List<DailyReport> dailyReports = new ArrayList<>(report.size());
        List<DailyReport> invalidRecords = new ArrayList<>();
        for (Map<String, String> map : report) {
            final DailyReport m = mapper.map(map, csvColumns);
            if (m == null) {
                continue;
            }

            m.setAdNetwork(adNetwork);

            if (isReportRecordValid(m)) {
                dailyReports.add(m);
            } else {
                invalidRecords.add(m);
            }
        }

        if (!dailyReports.isEmpty()) {
            // if we accidentally run import for the same date and Ad network multiple times,
            // let just remove records first, to avoid duplicated data
            final Date sqlDate = new Date(date.atStartOfDay(ZoneOffset.systemDefault()).toInstant().toEpochMilli());
            final Integer integer = dailyReportRepository.deleteAllByDateAndAdNetwork(sqlDate, adNetwork.getId());
            if (integer > 0) {
                log.info("It looks like the report already was ran for the same date. Removed {} old records before inserting new.", integer);
            }

            dailyReportRepository.saveAll(dailyReports);
            log.info("Saved {} report records for date {}.", dailyReports.size(), date);
        }

        return invalidRecords;
    }

    /**
     * Just simple check that report record is valid by checking its number of requests and impressions.
     * Number of requests couldn't be less (or even equal, not likely that it would happen) the number of impressions.
     * @param r - the instance of {@link DailyReport} to be checked.
     * @return true / false
     */
    private boolean isReportRecordValid(DailyReport r) {
        return r.getRequests() > r.getImpressions();
    }
}
