package com.apps7.adnetdatacollector.service.impl;


import com.apps7.adnetdatacollector.persistence.model.DailyReport;
import com.apps7.adnetdatacollector.service.ICsvSchemaToEntityMapper;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Map;

import static com.apps7.adnetdatacollector.Constants.UMBRELLA_NETWORK_NAME;

/**
 * One more implementation of the {@link ICsvSchemaToEntityMapper} to be able handle the reports from the
 * Umbrella AdNetwork.
 */
@Service(UMBRELLA_NETWORK_NAME)
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UmbrellaCsvToEntityMapper extends BaseCsvSchemaToEntityMapper {

    private static final String STOP_VALUE = "Totals";

    @Override
    public DailyReport map(Map<String, String> csvData, String[] csvColumns) {
        if (STOP_VALUE.equals(csvData.get(csvColumns[0]))) {
            return null;
        }

        return super.map(csvData, csvColumns);
    }
}
