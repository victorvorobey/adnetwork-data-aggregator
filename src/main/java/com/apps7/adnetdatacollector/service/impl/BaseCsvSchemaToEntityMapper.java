package com.apps7.adnetdatacollector.service.impl;

import com.apps7.adnetdatacollector.exception.ApplicationNotSupportedException;
import com.apps7.adnetdatacollector.exception.PlatformNotSupportedException;
import com.apps7.adnetdatacollector.persistence.model.DailyReport;
import com.apps7.adnetdatacollector.persistence.model.MobileApplication;
import com.apps7.adnetdatacollector.persistence.model.Platform;
import com.apps7.adnetdatacollector.persistence.repository.ApplicationRepository;
import com.apps7.adnetdatacollector.persistence.repository.PlatformRepository;
import com.apps7.adnetdatacollector.service.ICsvSchemaToEntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;

import static com.apps7.adnetdatacollector.Constants.BASE_CSV_TO_ENTITY_MAPPER;

/**
 * The class implements the basic logic of mapping the CSV report columns to the existing entity model which represents
 * the DailyReport. See the {@link DailyReport} model.
 * In case, if the implemented logic is not enough you can make own implementation by extending this class or implementing
 * the {@link ICsvSchemaToEntityMapper} interface.
 * For example see {@link UmbrellaCsvToEntityMapper}
 */
@Service(BASE_CSV_TO_ENTITY_MAPPER)
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BaseCsvSchemaToEntityMapper implements ICsvSchemaToEntityMapper {

    @Autowired
    private PlatformRepository platformRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    private DateTimeFormatter dateTimeFormatter;


    public DailyReport map(Map<String, String> csvData, final String[] csvColumns) {
        DailyReport model = new DailyReport();
        for (int i = 0; i < csvColumns.length; i++) {
            String csvColumn = csvColumns[i];
            final String s = csvData.get(csvColumn);

            switch (i) {
                case 0:
                    mapDate(model, s);
                    break;

                case 1:
                    mapApp(model, s);
                    break;

                case 2:
                    mapPlatform(model, s);
                    break;

                case 3:
                    mapRequests(model, s);
                    break;

                case 4:
                    mapImpressions(model, s);
                    break;

                case 5:
                    mapRevenue(model, s);
                    break;

                default: {
                    throw new RuntimeException("Unexpected row occurred");
                }
            }
        }

        return model;
    }

    protected void mapRevenue(DailyReport model, String s) {
        BigDecimal bigDecimal = new BigDecimal(s.replaceAll("[^\\\\.0123456789]", ""));
        model.setRevenue(bigDecimal.setScale(3, RoundingMode.HALF_EVEN));
    }

    protected void mapImpressions(DailyReport model, String s) {
        model.setImpressions(Long.valueOf(s));
    }

    protected void mapRequests(DailyReport model, String s) {
        model.setRequests(Long.valueOf(s));
    }

    protected void mapPlatform(DailyReport model, String s) {
        final Optional<Platform> byName = platformRepository.findByName(s.toUpperCase());
        if (!byName.isPresent()) {
            throw new PlatformNotSupportedException(String.format("Platform \"%s\" is not supproted", s));
        }
        model.setPlatform(byName.get());
    }

    protected void mapApp(DailyReport model, String s) {
        final Optional<MobileApplication> byName = applicationRepository.findByName(s.toUpperCase());
        if (!byName.isPresent()) {
            throw new ApplicationNotSupportedException(String.format("Application \"%s\" is not supported", s));
        }
        model.setApplication(byName.get());
    }

    protected void mapDate(DailyReport model, String val) {
        model.setDate(new Date(LocalDate.parse(val, dateTimeFormatter)
                .atStartOfDay(ZoneOffset.systemDefault()).toInstant().toEpochMilli()));
    }

    @Override
    public void setCSVDateFormat(String format) {
        dateTimeFormatter = DateTimeFormatter.ofPattern(format);
    }
}