package com.apps7.adnetdatacollector.service;

import com.apps7.adnetdatacollector.persistence.model.AdNetwork;
import com.apps7.adnetdatacollector.persistence.model.MobileApplication;
import com.apps7.adnetdatacollector.persistence.model.Platform;
import com.apps7.adnetdatacollector.persistence.repository.AdNetworkRepository;
import com.apps7.adnetdatacollector.persistence.repository.ApplicationRepository;
import com.apps7.adnetdatacollector.persistence.repository.PlatformRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Currency;

import static com.apps7.adnetdatacollector.Constants.UMBRELLA_NETWORK_NAME;

/**
 * The class initializes initial data in database on first start
 * like AdNetworks, MobileApplications, Platforms.
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @_(@Autowired))
@Service
public class InitService {

    private final AdNetworkRepository adNetworkRepository;
    private final ApplicationRepository applicationRepository;
    private final PlatformRepository platformRepository;

    @PostConstruct
    @Transactional
    public void initData() {
        initAdNetworks();
        initApplications();
        initPlatforms();
    }

    private void initAdNetworks() {
        final String superNetwork = "SuperNetwork";
        if (!adNetworkRepository.findByName(superNetwork).isPresent()) {
            AdNetwork superNetworkModel = new AdNetwork();
            superNetworkModel.setName(superNetwork);
            superNetworkModel.setDescription("SuperNetwork dummy description");
            superNetworkModel.setCsvSchema("Date,App,Platform,Requests,Impressions,Revenue");
            superNetworkModel.setCsvDateFormat("dd/M/yyyy");
            superNetworkModel.setReportUrlTemplate("https://storage.googleapis.com/expertise-test/supernetwork/report/daily/%s.csv");
            superNetworkModel.setReportUrlDateFormat("yyyy-MM-dd");
            superNetworkModel.setRevenueCurrency(Currency.getInstance("EUR"));

            adNetworkRepository.save(superNetworkModel);
            log.info("AdNetwork {} has been created", superNetwork);
        }

        if (!adNetworkRepository.findByName(UMBRELLA_NETWORK_NAME).isPresent()) {
            AdNetwork adUmbrellaNetModel = new AdNetwork();
            adUmbrellaNetModel.setName(UMBRELLA_NETWORK_NAME);
            adUmbrellaNetModel.setDescription("AdUmbrella dummy description");
            adUmbrellaNetModel.setCsvSchema("Date,App,Platform,Requests,Impressions,Revenue (usd)");
            adUmbrellaNetModel.setCsvDateFormat("dd/M/yyyy");
            adUmbrellaNetModel.setReportUrlTemplate("https://storage.googleapis.com/expertise-test/reporting/adumbrella/adumbrella-%s.csv");
            adUmbrellaNetModel.setReportUrlDateFormat("dd_M_yyyy");
            adUmbrellaNetModel.setRevenueCurrency(Currency.getInstance("USD"));

            adNetworkRepository.save(adUmbrellaNetModel);
            log.info("AdNetwork {} has been created", UMBRELLA_NETWORK_NAME);
        }
    }

    private void initApplications() {
        createDummyApplication("My Talking Ben".toUpperCase());
        createDummyApplication("My Talking Angela".toUpperCase());
        createDummyApplication("Talking Ginger".toUpperCase());
        createDummyApplication("My Talking Tom".toUpperCase());
    }

    private void createDummyApplication(final String name) {
        if (!applicationRepository.findByName(name).isPresent()) {
            MobileApplication m = new MobileApplication();
            m.setName(name);
            m.setDescription("Dummy description");
            applicationRepository.save(m);
        }
    }

    private void initPlatforms() {
        createDummyPlatform("IOS");
        createDummyPlatform("ANDROID");
    }

    private void createDummyPlatform(final String name) {
        if (!platformRepository.findByName(name).isPresent()) {
            Platform p = new Platform();
            p.setName(name);
            platformRepository.save(p);
        }
    }
}
