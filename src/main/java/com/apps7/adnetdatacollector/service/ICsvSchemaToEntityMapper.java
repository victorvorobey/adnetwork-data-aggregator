package com.apps7.adnetdatacollector.service;

import com.apps7.adnetdatacollector.persistence.model.DailyReport;
import org.springframework.lang.Nullable;

import java.util.Map;

public interface ICsvSchemaToEntityMapper {

    /**
     * The method maps the csv data to the {@link DailyReport} model.
     * May return null, in this case the value should be skipped.
     * @param csvData - the one row data from csv file to be mapped
     * @param csvColumns - the array of the csv columns
     * @return {@link DailyReport} or null
     */
    @Nullable
    DailyReport map(Map<String, String> csvData, final String[] csvColumns);

    void setCSVDateFormat(String format);
}
