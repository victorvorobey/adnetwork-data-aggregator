package com.apps7.adnetdatacollector;

import com.apps7.adnetdatacollector.persistence.model.DailyReport;
import com.apps7.adnetdatacollector.service.IDataImportService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;

/**
 * Bootstrap class using which spring boot is starting.
 */
@SpringBootApplication
@Slf4j
public class App implements CommandLineRunner {

    @Resource
    private IDataImportService importService;

    private static CommandLine cmd;

    public static void main(String[] args) {
        // Parse options
        Options options = new Options();

        Option adNetOpt = new Option("a", "ad-network", true, "The valid Advertising Network name the report will be pulled for.");
        adNetOpt.setRequired(true);
        options.addOption(adNetOpt);

        Option dateOpt = new Option("d", "date", true, "The date the report will be pulled for. Date format should be in ISO8601 format (yyyy-MM-dd).");
        dateOpt.setRequired(true);
        options.addOption(dateOpt);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("aggregator", options);
            System.exit(1);
            return;
        }

        SpringApplication app = new SpringApplication(App.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Context initialized");

        try {
            final LocalDate date = parseDate(cmd.getOptionValue("d"));
            final String adNetworkName = cmd.getOptionValue("a");

            log.info("Starting importing report for {} [{}]", adNetworkName, date);
            final List<DailyReport> invalidRecords = importService.importReport(adNetworkName, date);

            if (!invalidRecords.isEmpty()) {
                log.warn("{} invalid record(s) found:", invalidRecords.size());
                for (DailyReport invalidRecord : invalidRecords) {
                    log.warn("\t App:{}; Platform:{}; Requests:{}; Impressions:{}; Revenue:{}({})",
                            invalidRecord.getApplication().getName(),
                            invalidRecord.getPlatform().getName(),
                            invalidRecord.getRequests(),
                            invalidRecord.getImpressions(),
                            invalidRecord.getRevenue(),
                            invalidRecord.getAdNetwork().getRevenueCurrency()
                    );
                }
            }
        } catch (IllegalArgumentException e) {
            log.error("Invalid arguments.");
            log.error(e.getMessage());
            System.exit(1);
        }
    }

    private LocalDate parseDate(String date) {
        try {
            return LocalDate.parse(date);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("Can't parse date; Please use the following date format: yyyy-MM-dd. \"2017-09-15\" as example.");
        }
    }
}
