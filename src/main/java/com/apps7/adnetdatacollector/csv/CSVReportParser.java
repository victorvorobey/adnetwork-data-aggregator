package com.apps7.adnetdatacollector.csv;

import com.apps7.adnetdatacollector.persistence.model.AdNetwork;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

/**
 * The CSV parser which pulls data from remote url and parse it
 * according to passed column names.
 */
public class CSVReportParser {

    private String url;
    private String[] csvColumns;

    public CSVReportParser(final AdNetwork adNetwork, final LocalDate date, final String[] csvColumns) {
        final String datePattern = adNetwork.getReportUrlDateFormat();
        this.url = String.format(adNetwork.getReportUrlTemplate(), DateTimeFormatter.ofPattern(datePattern).format(date));
        this.csvColumns = csvColumns;
    }

    public List<Map<String, String>> run() throws IOException {
        final CsvSchema.Builder builder = CsvSchema.builder();

        // Add columns from SCV Schema
        for (String csvColumn : csvColumns) {
            builder.addColumn(csvColumn);
        }

        final CsvSchema schema = builder
                .setUseHeader(true)
                .setStrictHeaders(false).build()
                .withColumnSeparator(',')
                .withoutQuoteChar();

        CsvMapper mapper = (CsvMapper) new CsvMapper()
                .disable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);

        MappingIterator<Map<String, String>> mappingIterator = mapper
                        .readerFor(Map.class)
                        .with(schema)
                        .readValues(new URL(url));

        return mappingIterator.readAll();
    }
}
