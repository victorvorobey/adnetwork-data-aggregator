package com.apps7.adnetdatacollector.persistence.repository;

import com.apps7.adnetdatacollector.persistence.model.MobileApplication;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ApplicationRepository extends JpaRepository<MobileApplication, Integer> {

    String APPLICATION_BY_NAME_CACHE_KEY = "findApplicationByName";

    @Cacheable(APPLICATION_BY_NAME_CACHE_KEY)
    Optional<MobileApplication> findByName(String name);

    @Override
    @CacheEvict(APPLICATION_BY_NAME_CACHE_KEY)
    <S extends MobileApplication> S save(S s);
}
