package com.apps7.adnetdatacollector.persistence.repository;

import com.apps7.adnetdatacollector.persistence.model.DailyReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;

public interface DailyReportRepository extends JpaRepository<DailyReport, Long> {

    @Modifying
    @Query(value = "DELETE FROM daily_report where date = :date and ad_network_id = :adNetworkId", nativeQuery = true)
    Integer deleteAllByDateAndAdNetwork(@Param("date") Date date,
                                     @Param("adNetworkId") Integer adNetworkId);
}
