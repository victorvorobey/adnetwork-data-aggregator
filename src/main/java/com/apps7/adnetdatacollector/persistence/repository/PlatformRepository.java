package com.apps7.adnetdatacollector.persistence.repository;

import com.apps7.adnetdatacollector.persistence.model.Platform;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PlatformRepository extends JpaRepository<Platform, Integer> {

    String PLATFORM_BY_NAME_CACHE_KEY = "findPlatformByName";

    @Cacheable(PLATFORM_BY_NAME_CACHE_KEY)
    Optional<Platform> findByName(String name);

    @Override
    @CacheEvict(PLATFORM_BY_NAME_CACHE_KEY)
    <S extends Platform> S save(S s);
}
