package com.apps7.adnetdatacollector.persistence.repository;

import com.apps7.adnetdatacollector.persistence.model.AdNetwork;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdNetworkRepository extends JpaRepository<AdNetwork, Integer> {

    Optional<AdNetwork> findByName(String name);
}
