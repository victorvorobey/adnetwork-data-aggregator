package com.apps7.adnetdatacollector.persistence.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The class represents the Mobile Application entity.
 */
@Data
@Entity
@Table(name = "mobile_application",
        uniqueConstraints = {
                @UniqueConstraint(name = "application_name_UK", columnNames = "name")
        }
)
public class MobileApplication implements Serializable {

    private static final long serialVersionUID = 8721327424244538451L;

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(name = "native", strategy = "native")
    private Integer id;

    @Column(length = 50)
    private String name;

    private String description;
}
