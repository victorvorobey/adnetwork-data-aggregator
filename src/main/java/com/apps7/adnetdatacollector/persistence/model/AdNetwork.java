package com.apps7.adnetdatacollector.persistence.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Currency;

/**
 * The class represents an Advertising Network entity.
 */
@Data
@Entity
@Table(name = "ad_network",
        uniqueConstraints = {
                @UniqueConstraint(name = "ad_network_name_UK", columnNames = "name")
        }
)
public class AdNetwork implements Serializable {

    private static final long serialVersionUID = -3495765974778748156L;

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(name = "native", strategy = "native")
    private Integer id;

    @Column(length = 50)
    private String name;

    private String description;

    @Column(length = 100)
    private String csvSchema;

    @Column(length = 20)
    private String csvDateFormat;

    private String reportUrlTemplate;

    private String reportUrlDateFormat;

    private Currency revenueCurrency;
}
