package com.apps7.adnetdatacollector.persistence.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The class Represents the Platform entity.
 */
@Data
@Entity
@Table(name = "platform",
        uniqueConstraints = {
                @UniqueConstraint(name = "platform_name_UK", columnNames = "name")
        }
)
public class Platform implements Serializable {

    private static final long serialVersionUID = 8101229171132210439L;

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(name = "native", strategy = "native")
    private Integer id;

    @Column(length = 20)
    private String name;
}
