package com.apps7.adnetdatacollector.persistence.model;


import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * The class represents a Daily Report entity.
 */
@Entity
@Table(name = "daily_report")
@Data
public class DailyReport implements Serializable {

    private static final long serialVersionUID = 4018048595721083749L;

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @ManyToOne
    private AdNetwork adNetwork;

    @ManyToOne
    private MobileApplication application;

    @ManyToOne
    private Platform platform;

    private long requests;

    private long impressions;

    private BigDecimal revenue;

    private Date date;
}
