#!/usr/bin/env bash

PROFILE=$1
if [ -z $1 ]; then
PROFILE="prod"
fi

echo "Building profile: '${PROFILE}'"
mvn clean package -P ${PROFILE} -DskipTests=true -e