#!/usr/bin/env bash

# Building the app for the container profile
./build.sh container

# copy the jar into docker build context
cp ./target/aggregator.jar ./docker/javaapp/

# Building the image
cd ./docker/javaapp/
docker build -t aggregator-app .