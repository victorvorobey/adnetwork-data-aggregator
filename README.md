## Advertising Network Report Aggregator

This is an application that allows to pull daily reports from different Advertising 
Networks and store it in a database in a uniform way, so that it can be used
for analytical purposes later.
This application relies on an advertising network to provide reports with links to download them in CSV format. 
Out of the box the application supports 2 advertising networks: **SuperNetwork** and **AdUmbrella**.

---

### Supported formats

Currently the App supports CSV format only.
The CSV files must be provided with following columns:
`Date,App,Platform,Requests,Impressions,Revenue`
The column names are not strict -- the column names can be configured later in  the database.
---

### Prerequisites

This manual assumes that you work on a linux environment with docker installed.  
Also, to build the application you need to have Java (JDK) installed.  
The building process was tested with java version "1.8.0_171" 

---

### How to start

First, clone the repository  
```sh
$ git clone git@bitbucket.org:victorvorobey/adnetwork-data-aggregator.git
```
 
Enter into the the cloned directory  
```sh
$ cd adnetwork-data-aggregator
```

Run the `build-docker-image.sh` script
```sh
$ ./build-docker-image.sh
```
It will build docker image with name `aggregator-app`.
If you want to change db access details, you can do it by editing `container.properties` file before the build.

Run an empty docker container instance of MySQL server
```sh
$ docker run --name aggregator-db-mysql -t \
        -e MYSQL_DATABASE="aggregator" \
        -e MYSQL_USER="aggregator_user" \
        -e MYSQL_PASSWORD="aggregator" \
        -e MYSQL_ROOT_PASSWORD="pass$4root" \
        -d mysql:5.7 \
        --character-set-server=utf8 --collation-server=utf8_bin
```
Note, the default db access credentials from `container.properties` were applied here.
If you changed them in previous step, please, change them here as well.

Now, we can run the docker container for the `aggregator`.
```sh
$ docker run --rm --name aggregator \
    --link aggregator-db-mysql:mysql \
    aggregator-app:latest \
    java -jar aggregator.jar -a SuperNetwork -d 2017-09-15
```

Here,`SuperNetwork` and `2017-09-15` are the name of an Advertising Network and the report date appropriately.<br>
These options are mandatory to run this App.<br>

On the first run the application will initialize the database and put necessary data for the AdNetworks, which are being supported for now.
At the moment, there are two Advertising Networks available for import with following valid names:<br>
`SuperNetwork` and  `AdUmbrella`

Note, that the `aggregator` container doesn't store any state or data. All data will be saved
into the linked mysql container we created before, so it can be safely removed 
after execution. We do it by specifying `--rm` option.

---

### How to add new Advertising Network

All supported Advertising Networks are stored in the `ad_network` db table. 
In order to add a new Advertising Network, it's usually enough to add a new record into `ad_network` table.
But, if the report data to be imported from new Advertising Network has new platform names (currently IOS and Android are already in place) 
or new Mobile Applicaton / Game then, it's required to update the data in the `platform` and `mobile_application` db tables appropriately.
In case if the structure of a CSV file of new Advertising Network has not only columns header and the data itself, but 
also has some additional rows at the bottom, like `Totals` etc., the additional implementation of `ICsvSchemaToEntityMapper` is required (see source code).

---

### Notes on adding data into `ad_network` table

Here are some notes for some columns on how to add a new Advertising Network record properly:

1. column `report_url_template` - here should be placed the url template (without date) of fetching daily report.  
For example, if an AdNetwork provides reports with following url: `https://storage.googleapis.com/expertise-test/supernetwork/report/daily/2017-09-15.csv`,  
then, in db we should put `https://storage.googleapis.com/expertise-test/supernetwork/report/daily/%s.csv`.
Note, that date is replaced with `%s`.  
2. column `report_url_date_format` - the format of the date in the url from step above.  
In our case it will be `yyyy-MM-dd`.  
3. column `csv_date_format` - the format of the date from csv report. Example: `dd/M/yyyy`.  
4. column `csv_schema` - this column is important, and if do not fill it properly the application would not work.  
It should contain all existing comma separated column names from the csv report AdNewtork provided.  
Example:
```Date,App,Platform,Requests,Impressions,Revenue```
It doesn't matter in which order these columns are in the csv report itself.
But, the important thing is to keep the order of the columns by implication in db as in the example above.  
So, if a new report would have following column names:
```Application,Requests,ReportDate,Impressions,Revenue (usd),Platform```
then, in the db we should put them as follow:
```ReportDate,Application,Platform,Requests,Impressions,Revenue (usd)```  
5. column `name` - the name of the new AdNetwork.  
It can be whatever you want, but that's the name which the program expects as an argument (`-a` or `--ad-network`) when you run it.
